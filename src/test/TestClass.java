package test;

import model.Account;
import model.Person;
import model.SpendingAccount;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class TestClass {

    @Test
    public void testDeposit() {
        Account account = new SpendingAccount(5,new Person(4,"Alex Malutan"), "432423",300.0);
        account.deposit(100.0);

        assertEquals(account.getBalance(),400.0);
    }

    @Test
    public void testWithdraw() {
        Account account = new SpendingAccount(5,new Person(4,"Alex Malutan"), "432423",300.0);
        account.withdraw(100.0);

        assertEquals(account.getBalance(),200.0);
    }
}
