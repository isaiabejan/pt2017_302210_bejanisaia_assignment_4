package repository;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import model.Account;
import model.SavingAccount;
import model.SpendingAccount;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AccountRepository implements Repository<Account> {
    private List<Account> accounts;

    public AccountRepository() {
        this.accounts = new ArrayList<Account>();
    }

    public void add(Account account) {
        this.accounts.add(account);
    }

    public void remove(Account account) {
        this.accounts.remove(account);
    }

    public Account findById(Integer id) {
        for (Account account : accounts) {
            if (id.equals(account.getId())) {
                return account;
            }
        }

        return null;
    }

    public List<Account> findAll() {
        return this.accounts;
    }

    public void doImport() {
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            Account[] importedAccounts = mapper.readValue(new File("accounts.txt"), Account[].class);
            this.accounts = new ArrayList<Account>();
            for(Account account : importedAccounts) {
                if (account.getType().equals("spending account")) {
                     accounts.add(new SpendingAccount(account.getId(),account.getMainHolder(),account.getAccountNumber(), account.getBalance()));
                } else {
                    accounts.add(new SavingAccount(account.getId(),account.getMainHolder(),account.getAccountNumber(), account.getBalance()));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void doExport() {
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            mapper.writeValue(new File("accounts.txt"), this.accounts);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
