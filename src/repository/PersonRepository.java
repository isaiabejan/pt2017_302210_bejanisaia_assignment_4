package repository;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import model.Person;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class PersonRepository implements Repository<Person> {
    private List<Person> persons;

    public PersonRepository() {
        this.persons = new ArrayList<Person>();
    }

    public void add(Person person) {
        this.persons.add(person);
    }

    public void remove(Person person) {
        this.persons.remove(person);
    }

    public Person findById(Integer id) {
        for (Person person : persons) {
            if (id.equals(person.getId())) {
                return person;
            }
        }

        return null;
    }

    public List<Person> findAll() {
        return this.persons;
    }

    public void doImport() {
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            Person[] importedPersons = mapper.readValue(new File("persons.txt"), Person[].class);
            this.persons = new LinkedList<Person>(Arrays.asList(importedPersons));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void doExport() {
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            mapper.writeValue(new File("persons.txt"), this.persons);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
