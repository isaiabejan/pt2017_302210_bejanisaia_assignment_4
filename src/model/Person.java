package model;

import java.util.Observable;
import java.util.Observer;

public class Person implements Observer {
    private Integer id;
    private String name;

    public Person() {
    }

    public Person(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void update(Observable o, Object arg) {
        System.out.print("Account was updated " + arg + "\n");
    }
}
