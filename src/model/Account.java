package model;

import java.util.Observable;

public class Account extends Observable {
    private Integer id;
    private Person mainHolder;
    private String accountNumber;
    private Double balance;
    private String mainHolderName;
    private String type;

    public Account() {

    }

    public Account(Integer id, Person mainHolder, String accountNumber, String type, Double balance) {
        this.id = id;
        this.mainHolder = mainHolder;
        this.accountNumber = accountNumber;
        this.type = type;
        this.balance = balance;
        mainHolderName = mainHolder.getName();
        addObserver(mainHolder);
    }

    public Person getMainHolder() {
        return mainHolder;
    }

    public void setMainHolder(Person mainHolder) {
        this.mainHolder = mainHolder;
        this.mainHolderName = mainHolder.getName();
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setMainHolderName(String name) {
        mainHolderName = name;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public void deposit(Double amount) {
        balance += amount;
        setChanged();
        notifyObservers(this.getBalance());
    }

    public String getMainHolderName() {
        return mainHolderName;
    }

    public void setMainHolderId(Integer id) {
        mainHolder.setId(id);
    }

    public void withdraw(Double amount) {
        if (balance >= amount) {
            balance -= amount;
        } else {
            System.out.println("Not enough money");
        }
        setChanged();
        notifyObservers(this.getBalance());
    }


    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double amount) {
        balance = amount;
    }

    public Integer getId() {
        return id;
    }

    public String getType() {
        return this.type;
    }
}
