package model;

public class SavingAccount extends Account {
    private static final Double INTEREST = 0.1;
    private static final String TYPE = "saving account";
    private boolean canDeposit;
    private boolean canWithdraw;

    public SavingAccount(Integer id, Person mainHolder, String accountNumber, Double balance) {
        super(id, mainHolder, accountNumber, TYPE, balance);
        this.canDeposit = false;
        this.canWithdraw = false;

    }

    @Override
    public void deposit(Double amount) {
        if (!canDeposit) {
            this.setBalance(this.getBalance() + amount + amount * INTEREST);
            canDeposit = true;
        } else {
            return;
        }
        setChanged();
        notifyObservers(this.getBalance());

    }

    @Override
    public void withdraw(Double amount) {
        if(!canWithdraw) {
            if(this.getBalance() >= amount) {
                this.setBalance(this.getBalance() - amount);
                canWithdraw = true;
            }
        }
        setChanged();
        notifyObservers(this.getBalance());
    }

}
