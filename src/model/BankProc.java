package model;

public interface BankProc {
    /**
     * @pre person != NULL
     * @post clients.size() = clients@pre.size() + 1
     */
    public void addPerson(Person person);

    /**
     * @pre person != NULL
     * @post clients.size() = clients@pre.size() - 1
     */
    public void removePerson(Person person);

    /**
     * @pre person != NULL
     * @pre account != NULL;
     */
    public void addAccount(Account account);

    /**
     * @pre person != NULL
     * @pre account != NULL;
     */
    public void removeAccount(Account account);

    public void doExport();

    public void doImport();
}
