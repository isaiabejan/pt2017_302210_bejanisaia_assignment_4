package model;

public class SpendingAccount extends Account {
    private static final String TYPE = "spending account";
    private Integer balance;

    public SpendingAccount(Integer id, Person mainHolder, String accountNumber, Double balance ) {
        super(id, mainHolder, accountNumber, TYPE, balance);

    }

    public void deposit(Double amount) {
        this.setBalance(this.getBalance() + amount);
        setChanged();
        notifyObservers(this.getBalance());
    }

    public void withdraw(Double amount) {
        if(this.getBalance() >= amount) {
            this.setBalance(this.getBalance() - amount);
        } else {
            System.out.println("Not enough money");
        }
        setChanged();
        notifyObservers(this.getBalance());
    }
}
