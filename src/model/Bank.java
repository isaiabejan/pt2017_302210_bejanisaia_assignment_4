package model;

import repository.AccountRepository;
import repository.PersonRepository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Bank implements BankProc {
    private Map<Person, List<Account>> database;
    private PersonRepository personRepository;
    private AccountRepository accountRepository;

    public Bank() {
        this.database = new HashMap<Person, List<Account>>();
        this.personRepository = new PersonRepository();
        this.accountRepository = new AccountRepository();
        initializeDatabase();
    }

    private void initializeDatabase() {
        this.doImport();

        List<Account> accounts = accountRepository.findAll();
        for (Account account : accounts) {
            Person person = personRepository.findById(account.getMainHolder().getId());
            if (database.get(person) == null) {
                this.database.put(person, new ArrayList<Account>());
            }
            this.database.get(person).add(account);
        }
    }


    public void addPerson(Person person) {
        if (this.personRepository.findById(person.getId()) == null) {
            this.personRepository.add(person);

            this.personRepository.doExport();
            this.personRepository.doImport();
        }
    }

    public void removePerson(Person person) {
        this.personRepository.remove(person);

        this.personRepository.doExport();
        this.personRepository.doImport();
    }


    public void addAccount(Account account) {
        Person person = account.getMainHolder();
        if (database.get(person) == null) {
            this.database.put(person, new ArrayList<Account>());
        }
        this.database.get(person).add(account);

        if (this.accountRepository.findById(account.getId()) == null) {
            this.accountRepository.add(account);
        }
        if (this.personRepository.findById(person.getId()) == null) {
            this.personRepository.add(person);
        }

        this.accountRepository.doExport();
        this.accountRepository.doImport();

    }

    public void removeAccount(Account account) {
        this.accountRepository.remove(account);
    }

    public Account findAccountById(Integer id) {
        return this.accountRepository.findById(id);
    }

    public void doExport() {
        this.personRepository.doExport();
        this.accountRepository.doExport();
    }

    public void doImport() {
        this.personRepository.doImport();
        this.accountRepository.doImport();
    }

    public List<Person> getClients() {
        return personRepository.findAll();
    }

    public List<Account> getAccounts() {
        return accountRepository.findAll();
    }
}
