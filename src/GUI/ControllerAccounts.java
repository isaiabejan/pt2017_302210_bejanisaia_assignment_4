package GUI;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;

import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import model.*;


import java.net.URL;
import java.util.ResourceBundle;

public class ControllerAccounts implements Initializable{
    @FXML
    private TableView<Account> accountsTable;
    @FXML
    private TableColumn<Account, Integer> idColumn;
    @FXML
    private TableColumn<Account, String> nameColumn;
    @FXML
    private TableColumn<Account, String> accountNumberColumn;
    @FXML
    private TableColumn<Account, String> accountType;
    @FXML
    private TableColumn<Account, Double> balance;
    @FXML
    private Button add;
    @FXML
    private Button edit;
    @FXML
    private Button delete;
    @FXML
    private Button deposit;
    @FXML
    private Button withdraw;
    @FXML
    private TextField depositAmount;
    @FXML
    private TextField withdrawAmount;
    @FXML
    private TextField idField;
    @FXML
    private TextField mainHolderField;
    @FXML
    private TextField accNumberField;
    @FXML
    private TextField accTypeField;
    @FXML
    private TextField balanceField;
    @FXML
    private TextField idMHfield;

    private ObservableList<Account> data;

    private Bank bank = new Bank();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        Bank bank = new Bank();
        data = FXCollections.observableArrayList(bank.getAccounts());
        idColumn.setCellValueFactory(new PropertyValueFactory<Account, Integer>("id"));
        nameColumn.setCellValueFactory(new PropertyValueFactory<Account, String>("mainHolderName"));
        accountNumberColumn.setCellValueFactory(new PropertyValueFactory<Account, String>("accountNumber"));
        accountType.setCellValueFactory(new PropertyValueFactory<Account, String>("type"));
        balance.setCellValueFactory(new PropertyValueFactory<Account, Double>("balance"));
        accountsTable.setItems(data);
    }

    public void addAcc() {

        if(accTypeField.getText().equals("saving account")) {
            Person person = new Person(Integer.parseInt(idMHfield.getText()), mainHolderField.getText());
            Account account = new SavingAccount(Integer.parseInt(idField.getText()), person, accNumberField.getText(), Double.parseDouble(balanceField.getText()));
            bank.addAccount(account);
        } else {
            Person person = new Person(Integer.parseInt(idMHfield.getText()), mainHolderField.getText());
            Account account = new SpendingAccount(Integer.parseInt(idField.getText()), person, accNumberField.getText(),Double.parseDouble(balanceField.getText()));
            bank.addAccount(account);
        }



        data = FXCollections.observableArrayList(bank.getAccounts());
        accountsTable.setItems(data);

    }

    public void deleteAccount() {
        Account account = accountsTable.getSelectionModel().getSelectedItem();
        bank.removeAccount(account);

        data = FXCollections.observableArrayList(bank.getAccounts());
        accountsTable.setItems(data);
    }

    public void editAccount() {
//        Account account = accountsTable.getSelectionModel().getSelectedItem();
//        bank.removeAccount(account);
//        account.setId(Integer.parseInt(idField.getText()));
//        account.setMainHolderId(Integer.parseInt(idMHfield.getText()));
//        account.setMainHolderName(mainHolderField.getText());
//        account.setAccountNumber(accNumberField.getText());
//        account.setBalance(Double.parseDouble(balanceField.getText()));
//        account.setType(accountType.getText());
//        bank.addAccount(account);

        if(accTypeField.getText().equals("saving account")) {
            Person person = new Person(Integer.parseInt(idMHfield.getText()), mainHolderField.getText());
            Account account = accountsTable.getSelectionModel().getSelectedItem();
            bank.removeAccount(account);
            account = new SavingAccount(Integer.parseInt(idField.getText()), person, accNumberField.getText(), Double.parseDouble(balanceField.getText()));
            bank.addAccount(account);
        } else {
            Person person = new Person(Integer.parseInt(idMHfield.getText()), mainHolderField.getText());
            Account account = accountsTable.getSelectionModel().getSelectedItem();
            bank.removeAccount(account);
            account = new SpendingAccount(Integer.parseInt(idField.getText()), person, accNumberField.getText(), Double.parseDouble(balanceField.getText()));
            bank.addAccount(account);
        }

        data = FXCollections.observableArrayList(bank.getAccounts());
        accountsTable.setItems(data);
    }

    public void depositAmount() {
        Account account = accountsTable.getSelectionModel().getSelectedItem();
        //account.deposit(Double.parseDouble(depositAmount.getText()));
        bank.findAccountById(account.getId()).deposit(Double.parseDouble(depositAmount.getText()));

        bank.doExport();
        bank.doImport();

        data = FXCollections.observableArrayList(bank.getAccounts());
        accountsTable.setItems(data);
    }

    public void withdrawAmount() {
        Account account = accountsTable.getSelectionModel().getSelectedItem();
        //bank.findAccountById(account.getId());
        bank.findAccountById(account.getId()).withdraw(Double.parseDouble(withdrawAmount.getText()));
        bank.doExport();
        bank.doImport();

        data = FXCollections.observableArrayList(bank.getAccounts());
        accountsTable.setItems(data);
    }


}
