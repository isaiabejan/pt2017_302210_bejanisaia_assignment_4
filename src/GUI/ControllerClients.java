package GUI;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;

import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import model.Bank;
import model.Person;
import repository.PersonRepository;


import java.net.URL;
import java.util.ResourceBundle;


public class ControllerClients implements Initializable {
    @FXML
    private TableView<Person> clientsTable;
    @FXML
    private TableColumn<Person, Integer> idColumn;
    @FXML
    private TableColumn<Person, String> nameColumn;
    @FXML
    private Button add;
    @FXML
    private Button edit;
    @FXML
    private Button delete;
    @FXML
    private TextField idField;
    @FXML
    private TextField nameField;

    private ObservableList<Person> data;

    private Bank bank = new Bank();


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //Bank bank = new Bank();
        data = FXCollections.observableArrayList(bank.getClients());

        idColumn.setCellValueFactory(new PropertyValueFactory<Person, Integer>("id"));
        nameColumn.setCellValueFactory(new PropertyValueFactory<Person, String>("name"));
        clientsTable.setItems(data);

    }

    public void addClient() {
        Person person = new Person(Integer.parseInt(idField.getText()), nameField.getText());
        bank.addPerson(person);

        data = FXCollections.observableArrayList(bank.getClients());
        clientsTable.setItems(data);

    }

    public void deleteClient() {
        Person person = clientsTable.getSelectionModel().getSelectedItem();
        bank.removePerson(person);

        data = FXCollections.observableArrayList(bank.getClients());
        clientsTable.setItems(data);
    }

    public void editClient() {
        Person person = clientsTable.getSelectionModel().getSelectedItem();
        bank.removePerson(person);
        person.setId(Integer.parseInt(idField.getText()));
        person.setName(nameField.getText());
        bank.addPerson(person);

        data = FXCollections.observableArrayList(bank.getClients());
        clientsTable.setItems(data);
    }
}
